# Thinkfan restart

## Purpose
I was having an issue where the cooling fan on my ThinkPad x201 was quitting,
randomly, and my laptop was overheating.  So, I created this script to check if
the cooling system had quit for some reason, and restart it if true.

It checks every ten minutes: if the cooling system is still running, nothing
happens; if the cooling system isn't running, during the check, the cooling
system is restarted.

## Requirements
Before using these scripts, it is important that you have Cron installed, as well
as the ThinkFan module installed.

This repo, and script, has been designed for simplicity, and should be modular
enough to work with Cron and any other module (replacing ThinkFan).

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/thinkfan_restart.git

## First thing's first...
This is designed to check your cooling system's status, every ten minutes.  This
is accomplished through a Cron job, and can be altered in the `setup.sh` file.
There is an obvious Cron job in line 5, which can be altered to make the script
run more, or less, frequently.

If you do not wish to change the 10 minute frequency, you do not have to alter
this file, in any way.

## Installing and enabling
Once that is all done, you will need to make the `setup.sh` file executable:

    chmod +x setup.sh

Now, you can run `./setup.sh` and enter your password.  This script will move the
appropriate files, make them executable, and create the Cron job to run at the
specified frequency, as above in the "First thing's first..." section.

